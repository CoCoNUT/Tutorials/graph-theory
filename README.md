* Totorials for hands-on session on graph theory with Python at an entry-level. 
* <a href= "https://www.cbs.mpg.de/en/cbs-coconut">CBS CoCoNUT @ MPI CBS</a>, April 2021


* This material is licensed under the terms of the GNU General Public License.

Session by [Mina Jamshidi](https://minajamshidi.github.io/), see also her [GitHub](https://github.com/minajamshidi)
